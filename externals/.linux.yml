include:
  - remote: 'https://git.codelinaro.org/linaro/qcomlt/ci/configs/-/raw/master/externals/.remotes.yml'

workflow:
  name: '$CONFIGS_PIPELINE'
  # only create pipeline for certain branches
  rules:
    - if: $CI_COMMIT_BRANCH =~ /master/
    - if: $CI_COMMIT_BRANCH =~ /release\/qcomlt-/
    - if: $CI_COMMIT_BRANCH == "integration-linux-qcomlt"

default:
  image: !reference [.linux_resources, image]
  tags:
    - !reference [.linux_resources, tags]
  before_script: |
    set -ex
    export KERNEL_TEST_FLAVOR=linux-${KERNEL_FLAVOR}
    export KERNEL_BRANCH="$CI_COMMIT_BRANCH"

variables:
  KERNEL_REPO_URL:
    value: '$CI_PROJECT_URL'
    description: 'Qualcomm kernel repository'
  KERNEL_CONFIGS_arm64:
    value: 'defconfig'
    description: 'ARM64 kernel configs'
  KERNEL_CONFIGS_arm:
    value: 'multi_v7_defconfig'
    description: 'ARM kernel configs'
  KERNEL_BUILD_TARGET:
    value: 'deb-pkg'
    description: ''
  KERNEL_DESCRIBE:
    value: ''
    description: ''
  KERNEL_VERSION:
    value: 'mainline'
    description: 'kernel version'
  KERNEL_FLAVOR:
    value: 'mainline'
    description: 'Kernel flavor'
  KERNEL_DEBIAN:
    value: ''
    description: ''
  TARGET_REPO:
    value: 'linaro-overlay-sid'
    description: ''
  KDEB_CHANGELOG_DIST:
    value: 'sid'
    description: ''
  UPDATE_DEFCONFIG:
    value: 'True'
    description: ''
  KERNEL_UPSTREAM_MAINLINE:
    value: 'https://kernel.googlesource.com/pub/scm/linux/kernel/git/torvalds/linux.git'
    description: 'Mainline upstream kernel repository'
  KERNEL_UPSTREAM_STABLE:
    value: 'https://kernel.googlesource.com/pub/scm/linux/kernel/git/stable/linux-stable.git'
    description: 'Stable upstream kernel repository'
  QA_SERVER_PROJECT:
    value: 'linux-selftest'
    description: ''
  BOOTRR_GIT_REPO:
    value: 'https://github.com/andersson/bootrr.git'
    description: ''
  BOOTRR_GIT_BRANCH:
    value: 'master'
    description: ''

  ARM_TOOLCHAIN: 'https://armkeil.blob.core.windows.net/developer/Files/downloads/gnu-a/10.2-2020.11/binrel/gcc-arm-10.2-2020.11-x86_64-arm-none-eabi.tar.xz'
  ARM64_TOOLCHAIN: 'https://armkeil.blob.core.windows.net/developer/Files/downloads/gnu-a/10.2-2020.11/binrel/gcc-arm-10.2-2020.11-x86_64-aarch64-none-linux-gnu.tar.xz'
  KERNEL_REPO_DIR: '$CI_PROJECT_DIR/kernel-ci' # in case of repository selftest, clone the kernel at the latter dir
  CONFIGS_PIPELINE: 'linux'

stages:
  - build
  - test

linux:arm:
  variables: !reference [.linux_resources, variables]
  stage: build
  script:
    - bash -x <(curl -fsSL $CONFIGS_RAW_URL/scripts/linux/build.sh)
    - touch $CI_PROJECT_DIR/CI_JOB_STATUS_SUCCESS
  extends:
      - .send-email-on-failure
  variables:
    ARCH: arm
  rules:
    - if: $CI_COMMIT_BRANCH =~ /release\/qcomlt-/
      variables:
        KERNEL_DEBIAN: 'linux'
  artifacts:
    paths:
      - kernel_parameters
      - kernel_pub_dest_parameters
      - kernel_test_params
      - kernel_params

linux:arm64:
  variables: !reference [.linux_resources, variables]
  stage: build
  script:
    - bash -x <(curl -fsSL $CONFIGS_RAW_URL/scripts/linux/build.sh)
    - touch $CI_PROJECT_DIR/CI_JOB_STATUS_SUCCESS
  extends:
      - .send-email-on-failure
  variables:
    ARCH: arm64
  rules:
    - if: $CI_COMMIT_BRANCH =~ /release\/qcomlt-/
      variables:
        KERNEL_DEBIAN: 'linux'
  artifacts:
    paths:
      - kernel_parameters
      - kernel_pub_dest_parameters
      - kernel_test_params
      - kernel_params

linux:mainline:
  variables: !reference [.linux_resources, variables]
  stage: test
  script:
    - bash -x <(curl -fsSL $CONFIGS_RAW_URL/scripts/linux/test.sh)
    - touch $CI_PROJECT_DIR/CI_JOB_STATUS_SUCCESS
  extends:
      - .send-email-on-failure
  parallel:
    matrix:
      - MACHINE:
          - apq8016-sbc
          - apq8096-db820c
          - msm8998-mtp
          - qcs404-evb-1000
          - qcs404-evb-4000
          - qrb5165-rb5
          - sm8150-mtp
          - sm8250-mtp
          - sm8350-mtp
          - sc7180-idp
          - sdm845-db845c
          - sdm845-mtp
  needs: ["linux:arm64"]
  rules:
    - if: $CI_COMMIT_BRANCH =~ /master/
      variables:
        KERNEL_VERSION: 'mainline'
        KERNEL_FLAVOR: 'mainline'
        KERNEL_CONFIGS_arm64: 'defconfig'
        KERNEL_CONFIGS_arm: 'multi_v7_defconfig'
        QA_SERVER_PROJECT: 'linux-master'
  artifacts:
    paths:
      - image_pub_dest_parameters
      - image_test_params
      - image_out_folder_files

linux:integration:
  variables: !reference [.linux_resources, variables]
  stage: test
  script:
    - bash -x <(curl -fsSL $CONFIGS_RAW_URL/scripts/linux/test.sh)
    - touch $CI_PROJECT_DIR/CI_JOB_STATUS_SUCCESS
  extends:
      - .send-email-on-failure
  parallel:
    matrix:
      - MACHINE:
          - apq8016-sbc
          - apq8096-db820c
          - msm8998-mtp
          - qcs404-evb-1000
          - qcs404-evb-4000
          - qrb5165-rb5
          - sm8150-mtp
          - sm8250-mtp
          - sm8350-mtp
          - sc7180-idp
          - sdm845-db845c
          - sdm845-mtp
  needs: ["linux:arm64"]
  rules:
    - if: $CI_COMMIT_BRANCH == "integration-linux-qcomlt"
      variables:
        KERNEL_VERSION: 'integration'
        KERNEL_FLAVOR: 'integration'
        QA_SERVER_PROJECT: 'linux-integration'
  artifacts:
    paths:
      - image_pub_dest_parameters
      - image_test_params
      - image_out_folder_files

linux:release:
  variables: !reference [.linux_resources, variables]
  stage: test
  before_script:
    - KERNEL_VERSION="${CI_COMMIT_BRANCH#release/qcomlt-}"
  script:
    - bash -x <(curl -fsSL $CONFIGS_RAW_URL/scripts/linux/test.sh)
    - touch $CI_PROJECT_DIR/CI_JOB_STATUS_SUCCESS
  extends:
      - .send-email-on-failure
  parallel:
    matrix:
      - MACHINE:
          - apq8016-sbc
          - sdm845-db845c
          - qrb5165-rb5
  needs: ["linux:arm64"]
  variables:
    KERNEL_FLAVOR: 'release'
    KERNEL_DEBIAN: 'linux'
    QA_SERVER_PROJECT: 'linux-release'
  rules:
    - if: $CI_COMMIT_BRANCH =~ /release\/qcomlt-/
  artifacts:
    paths:
      - image_pub_dest_parameters
      - image_test_params
      - image_out_folder_files
