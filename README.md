# Linaro Qualcomm Landing CI

Repository that host pipeline configurations (where their definitions are written in `yaml` format and
commonly called *configs* for Pipeline Configurations, following the **pipeline as code** practice )
for the Linaro Qualcomm Landing team.

## Pipeline types

There are two types of pipelines, located in separate folders:

- `polls`: the polling pipelines look for project(s) updates and if there are, it tracks the change
   and git-push it to the corresponding external repository, where the latter will use the pipeline
   configuration located at the `externals` folder.
- `externals`: the external pipelines are consumed by other projects, e.g. `kernel`, which in turn
   are triggered by `polls` pipelines.

## Pipeline schedules

Poll pipelines are triggered on a scheduled basis, some daily and some hourly. See
[scheduled pipelines](https://git.codelinaro.org/linaro/qcomlt/ci/configs/-/pipeline_schedules) for details.

## External repositories

Currently, external repositories are located at [linaro/qcomlt/ci](https://git.codelinaro.org/linaro/qcomlt/ci).
For example, pipeline `polls/linux-automerge` polls for new changes and if there are, it pushes to
[linaro/qcomlt/ci/linux-automerge](https://git.codelinaro.org/linaro/qcomlt/ci/linux-automerge)

## Pipeline Status

In case of job failure, an email reporting the failure is sent to people defined in the `TO_EMAILS` variable.
Also, when a a external pipeline executes succesfully, the latter status is also sent through email.
