set -ex

install_toolchain () {
   if [ -z "${ARCH}" ]; then
       ARCH=arm64
   fi
   if [ "${TOOLCHAIN_ARCH}" ]; then
       ARCH="${TOOLCHAIN_ARCH}"
   fi

   toolchain_url_arm=${ARM_TOOLCHAIN}
   toolchain_url_arm64=${ARM64_TOOLCHAIN}
   toolchain_url=toolchain_url_$ARCH
   toolchain_url=${!toolchain_url}

   export tcdir=${HOME}/srv/toolchain
   export tcbindir="${tcdir}/$(basename $toolchain_url .tar.xz)/bin"
   if [ ! -d "${tcbindir}" ]; then
       wget "${toolchain_url}"
       sudo mkdir -p "${tcdir}"
       sudo tar -xvf "$(basename ${toolchain_url})" -C "${tcdir}"
       rm -v "$(basename ${toolchain_url})"
   fi

   export PATH=$tcbindir:$PATH
}

fetch_kernel_tags () {
    cd $CI_PROJECT_DIR
    time git fetch --tags ${KERNEL_UPSTREAM_MAINLINE}
    time git fetch --tags ${KERNEL_UPSTREAM_STABLE}
}

set_kernel_variables () {
    cd $CI_PROJECT_DIR
    KERNEL_VERSION=$(make kernelversion)
    KERNEL_DESCRIBE=$(git describe --always)
    TARGET_REPO=linaro-overlay-sid
    KDEB_CHANGELOG_DIST=unstable
    KERNEL_CONFIGS_arm="$KERNEL_CONFIGS_arm"
    KERNEL_CONFIGS_arm64="$KERNEL_CONFIGS_arm64"
}

build_linux () {
    cd $CI_PROJECT_DIR
    KERNEL_COMMIT="$(git rev-parse HEAD)"
    if [ -z "${ARCH}" ]; then
    export ARCH=arm64
    export KERNEL_CONFIGS_arm64="defconfig distro.config"
    fi
    if [ -z "${KERNEL_VERSION}" ]; then
    KERNEL_VERSION=$(make kernelversion)
    fi
    if [ -z "${KERNEL_DESCRIBE}" ]; then
    time git fetch --tags ${KERNEL_UPSTREAM_MAINLINE}
    time git fetch --tags ${KERNEL_UPSTREAM_STABLE}
    KERNEL_DESCRIBE=$(git describe --always)
    fi
    if [ -z "${KDEB_CHANGELOG_DIST}" ]; then
    KDEB_CHANGELOG_DIST="unstable"
    fi
    if [ -z "${KERNEL_BUILD_TARGET}" ]; then
    KERNEL_BUILD_TARGET="deb-pkg"
    fi
    KERNEL_CONFIGS=KERNEL_CONFIGS_$ARCH

    # tcbindir from install-gcc-toolchain.sh
    toolchain="$(basename $(ls -1 ${tcbindir}/*-gcc))"
    export CROSS_COMPILE="ccache $(basename $toolchain gcc)"
    export PATH=${tcbindir}:$PATH
    KERNEL_TOOLCHAIN="$(ccache $toolchain --version | head -1)"

    cat << EOF > $CI_PROJECT_DIR/kernel_parameters
KERNEL_REPO_URL="${KERNEL_REPO_URL}"
KERNEL_COMMIT="${KERNEL_COMMIT}"
KERNEL_BRANCH="${KERNEL_BRANCH}"
KERNEL_CONFIG="${!KERNEL_CONFIGS}"
KERNEL_VERSION="${KERNEL_VERSION}"
KERNEL_DESCRIBE="${KERNEL_DESCRIBE}"
KERNEL_TOOLCHAIN="${KERNEL_TOOLCHAIN}"
EOF

    echo "Starting ${CI_JOB_STAGE}-${CI_JOB_NAME} with the following parameters:"
    cat $CI_PROJECT_DIR/kernel_parameters

    # SRCVERSION is the main kernel version, e.g. <version>.<patchlevel>.0.
    # PKGVERSION is similar to make kernelrelease, but reimplemented, since it requires setting up the build (and all tags).
    # e.g. SRCVERSION -> 4.9.0, PKGVERSION -> 4.9.47-530-g244b81e58a54, which leads to
    #      linux-4.9.0-qcomlt (4.9.47-530-g244b81e58a54-99)
    SRCVERSION=$(echo ${KERNEL_VERSION} | sed 's/\(.*\)\..*/\1.0/')
    PKGVERSION=$(echo ${KERNEL_VERSION} | sed -e 's/\.0-rc/\.0~rc/')$(echo ${KERNEL_DESCRIBE} | awk -F- '{printf("-%05d-%s", $(NF-1),$(NF))}')

    make distclean
    make ${!KERNEL_CONFIGS}
    if [ "${UPDATE_DEFCONFIG}" ]; then
	make savedefconfig
	cp defconfig arch/${ARCH}/configs
    fi

    # We only want to build deb OBS packages on release branches not in linux-integration and linux-mainline.
    if [ "${KERNEL_DEBIAN}" ]; then
	ls -la
	make KERNELRELEASE=${SRCVERSION}-qcomlt-${ARCH} \
	     KDEB_SOURCENAME=${KERNEL_DEBIAN}-${SRCVERSION}-qcomlt-${ARCH} \
	     KDEB_PKGVERSION=${PKGVERSION}-${CI_PIPELINE_ID} \
	     KDEB_CHANGELOG_DIST=${KDEB_CHANGELOG_DIST} \
	     DEBEMAIL="dragonboard@lists.96boards.org" \
	     DEBFULLNAME="Linaro Qualcomm Landing Team" \
	     -j$(nproc) ${KERNEL_BUILD_TARGET}

	cat > $CI_PROJECT_DIR/kernel_params <<EOF
source=${JOB_URL}/ws/$(echo ../*.dsc | sed 's/\.\.\///')
repo=${TARGET_REPO}
EOF
    else
	make KERNELRELEASE=${SRCVERSION}-qcomlt-${ARCH} -j$(nproc)
    fi

    make KERNELRELEASE=${SRCVERSION}-qcomlt-${ARCH} -j$(nproc) INSTALL_MOD_STRIP=1 INSTALL_MOD_PATH=./INSTALL_MOD_PATH modules_install
}

set_test_variables () {
    cd $CI_PROJECT_DIR
    KERNEL_OUT_DIR=$CI_PROJECT_DIR/kernel_out
    mkdir -p $KERNEL_OUT_DIR
    (cd $CI_PROJECT_DIR/INSTALL_MOD_PATH && find . | cpio -R 0:0 -ov -H newc | gzip > $KERNEL_OUT_DIR/kernel-modules.cpio.gz)
    (cd $CI_PROJECT_DIR/INSTALL_MOD_PATH && tar cJvf $KERNEL_OUT_DIR/kernel-modules.tar.xz .)
    cp $CI_PROJECT_DIR/.config $KERNEL_OUT_DIR/kernel.config
    cp $CI_PROJECT_DIR/{System.map,vmlinux} $KERNEL_OUT_DIR/
    cp $CI_PROJECT_DIR/arch/$ARCH/boot/Image* $KERNEL_OUT_DIR/
    (mkdir -p $KERNEL_OUT_DIR/dtbs && cd $CI_PROJECT_DIR/arch/$ARCH/boot/dts && cp -a --parents $(find . -name '*.dtb') $KERNEL_OUT_DIR/dtbs)

    if [ "${KERNEL_DEBIAN}" ]; then
        cp $CI_PROJECT_DIR/../{*.dsc,*.orig.tar.*,*.diff.gz} $KERNEL_OUT_DIR/
    fi

    cat > $KERNEL_OUT_DIR/HEADER.textile << EOF
h4. QC LT kernel build

Build description:
* Build URL: "$CI_JOB_URL":$CI_JOB_URL
* KERNEL_REPO: ${KERNEL_REPO_URL}
* KERNEL_COMMIT: ${KERNEL_COMMIT}
* KERNEL_BRANCH: ${KERNEL_BRANCH}
* KERNEL_CONFIG: ${!KERNEL_CONFIGS}
* KERNEL_VERSION: ${KERNEL_VERSION}
* KERNEL_DESCRIBE: ${KERNEL_DESCRIBE}
* KERNEL_TOOLCHAIN: ${KERNEL_TOOLCHAIN}
EOF
    cat $KERNEL_OUT_DIR/HEADER.textile

    # Set publication destination into snapshots.linaro.org
    BRANCH_NAME_URL=$(echo ${KERNEL_BRANCH} | sed -e 's/[^A-Za-z0-9._-]/_/g')
    PUB_DEST="${PUB_DEST_PREFIX}member-builds/qcomlt/kernel/${BRANCH_NAME_URL}/${CI_PIPELINE_ID}/${ARCH}"

    # pub_dest_parameters is saved as artifact
    echo "PUB_DEST=${PUB_DEST}" > $CI_PROJECT_DIR/kernel_pub_dest_parameters

    # Create test_params to trigger linux-test- builds only for arm64 builds
    if [ "${ARCH}" = "arm64" ]; then
	cat > $CI_PROJECT_DIR/kernel_test_params << EOF
KERNEL_IMAGE_URL="${PUBLISH_SERVER}/${PUB_DEST}/Image"
KERNEL_MODULES_URL="${PUBLISH_SERVER}/${PUB_DEST}/kernel-modules.tar.xz"
KERNEL_DT_URL="${PUBLISH_SERVER}/${PUB_DEST}/dtbs"
KERNEL_REPO_URL="${KERNEL_REPO_URL}"
KERNEL_COMMIT="${KERNEL_COMMIT}"
KERNEL_BRANCH="${KERNEL_BRANCH}"
KERNEL_CONFIG="${!KERNEL_CONFIGS}"
KERNEL_VERSION="${KERNEL_VERSION}"
KERNEL_DESCRIBE="${KERNEL_DESCRIBE}"
KERNEL_TOOLCHAIN="${KERNEL_TOOLCHAIN}"
EOF
    fi

}

publish_artifacts () {
    # Publish based on PUBLISH_ARTIFACTS variable
    if [[ "$PUBLISH_ARTIFACTS" == "false" ]]; then
	exit
    fi

    if [ ! -d "$KERNEL_OUT_DIR" ]; then
        echo "Avoid publishing, not $KERNEL_OUT_DIR directory exists."
        exit 0
    fi

    # Create MD5SUMS file
    (cd $KERNEL_OUT_DIR && md5sum $(find . -type f) > MD5SUMS.txt)

    time python3 $(which linaro-cp.py) \
	 --server ${PUBLISH_SERVER} \
	 --link-latest \
	 $KERNEL_OUT_DIR ${PUB_DEST}

    echo $PUB_DEST >$CI_PROJECT_DIR/image_pub_dest_parameters
}

## main
install_toolchain
fetch_kernel_tags
set_kernel_variables
build_linux
set_test_variables
publish_artifacts
