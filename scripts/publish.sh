# Publish based on PUBLISH_ARTIFACTS variable
if [[ "$PUBLISH_ARTIFACTS" == "false" ]]; then
    exit
fi

time python3 $(which linaro-cp.py) --server "${PUBLISH_SERVER}" "${PUB_PARAMS}" "${PUB_DIR}" "${PUB_DEST}"
