set -ex

cat << EOF > ${HOME}/qcom.sshconfig
Host staging-git.codelinaro.org
    UserKnownHostsFile /dev/null
    StrictHostKeyChecking no
EOF

chmod 400 "$LT_QUALCOMM_STAGING_CLO_PRIVATE_KEY_SSH"
ssh-add "$LT_QUALCOMM_STAGING_CLO_PRIVATE_KEY_SSH"

chmod 0600 ${HOME}/qcom.sshconfig

git config --global user.name "$CI_SERVER_HOST CI"
git config --global user.email "ci_notify@codelinaro.org"
git config --global core.sshCommand "ssh -F ${HOME}/qcom.sshconfig"
